#include "stdio.h"

#include "stm32f10x.h"
#include "stm32f10x_tim.h"
#include "stm32f10x_usart.h"

#include "mw_soft_timer.h"
#include "mw_printf.h"

void TIM2_IRQHandler(void) // TIM3 interrupt
{
    if(TIM_GetITStatus(TIM2, TIM_IT_Update) != RESET)
    {
        // 硬件置了1，需要软件来清0
        TIM_ClearITPendingBit(TIM2, TIM_IT_Update);
        /* add your code here */
        mw_soft_timer_user_systick_update();
    }
}

void USART1_IRQHandler(void)
{
    // uint16_t usart_tx_data;
    uint8_t usart_rx_data;

    
    if(USART_GetITStatus(USART1, USART_IT_TC) != RESET)
    {
        // USART_SendData(USART1, )
        USART_ClearITPendingBit(USART1, USART_IT_TC);		// clear flag
    }

    if(USART_GetITStatus(USART1, USART_IT_RXNE) != RESET)
    {
        // USART_SendData(USART1, )
        USART_ClearITPendingBit(USART1, USART_IT_RXNE);		// clear flag
        usart_rx_data = (uint8_t)(USART_ReceiveData(USART1));
        mw_printf_insert_data(usart_rx_data);
        
    }
}
