#ifndef __BSP_USART_H__
#define __BSP_USART_H__

#include "stdint.h"

typedef enum
{
    usart0_enum = 0,
    usart1_enum = 1,
    usart2_enum = 2,
    usart3_enum = 3,
    usart4_enum = 4,
    usart5_enum = 5,
    usart_cnt_enum = 6
}usart_type_Enum;


void bsp_usart_1_init(uint32_t baud);

void bsp_usart_send_data(usart_type_Enum e_usart_type, uint16_t us_tx_data);

uint16_t bsp_usart_receive_data(usart_type_Enum e_usart_type);

#endif

