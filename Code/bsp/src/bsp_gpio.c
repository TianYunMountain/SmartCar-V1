#include "bsp_gpio.h"
// #include "stm32f10x_gpio.h"
// #include "stm32f10x_rcc.h"
// #include "stm32f10x.h"
// #include "stdint.h"





// typedef struct
// {
//     GPIO_TypeDef* GPIOx;
//     uint16_t GPIO_Pin;

//     /* data */
// }GPIO_USER_TYPE_STR;

// static GPIO_USER_TYPE_STR bsp_gpio_user_type_buf[e_GPIO_NUM] =
// {
//     [e_GPIO_LED0] = {GPIOA, GPIO_Pin_8},
//     [e_GPIO_LED1] = {GPIOD, GPIO_Pin_2},
// }




// static GPIO_CONFIG_STR bsp_gpio_user_type_buf[e_GPIO_NUM];

// GPIO_CONFIG_STR bsp_gpio_get_gpio_type_buf(GPIO_USER_TYPE_ENUM e_val)
// {
//     // 是否越界
//     if(e_val >= e_GPIO_NUM)
//     {
//         while(1);
//     }
//     return bsp_gpio_user_type_buf[e_val];
// }


// static void bsp_gpio_led0_init(void)
// {
//     GPIO_InitTypeDef GPIO_InitStructure;
    
//     RCC_APB2PeriphClockCmd(IO_LED0_RCC_PERIPH, ENABLE); //使能 PA 端口时钟

//     GPIO_InitStructure.GPIO_Pin = IO_LED0_PIN;           //LED0-->PA.8 端口配置
//     GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;    //推挽输出
//     GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;   //IO 口速度为 50MHz
//     GPIO_Init(IO_LED0_PORT, &GPIO_InitStructure);              //初始化 GPIOA.8
//     // 初始化：高
//     GPIO_SetBits(IO_LED0_PORT, IO_LED0_PIN);                     //PA.8 输出高
// }
// // LED0 高电平
// static void bsp_gpio_set_led0_high(void)
// {
//     GPIO_SetBits(IO_LED0_PORT, IO_LED0_PIN);
// }
// // LED0 低电平
// static void bsp_gpio_set_led0_low(void)
// {
//     GPIO_ResetBits(IO_LED0_PORT, IO_LED0_PIN);
// }

// // LED1 初始化
// static void bsp_gpio_led1_init(void)
// {
//     GPIO_InitTypeDef GPIO_InitStructure;
    
//     RCC_APB2PeriphClockCmd(IO_LED1_RCC_PERIPH, ENABLE); //使能 PD 端口时钟

//     GPIO_InitStructure.GPIO_Pin = IO_LED1_PIN;           //LED0-->PD.2 端口配置
//     GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;    //推挽输出
//     GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;   //IO 口速度为 50MHz
//     GPIO_Init(IO_LED1_PORT, &GPIO_InitStructure);              //初始化 GPIOD.2
//     // 初始化：高
//     GPIO_SetBits(IO_LED1_PORT, IO_LED1_PIN);                     //PD.2 输出高
// }
// // LED1 高电平
// static void bsp_gpio_set_led1_high(void)
// {
//     GPIO_SetBits(IO_LED1_PORT, IO_LED1_PIN);
// }
// // LED1 低电平
// static void bsp_gpio_set_led1_low(void)
// {
//     GPIO_ResetBits(IO_LED1_PORT, IO_LED1_PIN);
// }

// /* bsp_gpio初始化函数 */
// void bsp_gpio_init(void)
// {
//     uint8_t tmp_i;
//     /* led0 */
//     bsp_gpio_user_type_buf[e_GPIO_LED0].init_func = bsp_gpio_led0_init;
//     bsp_gpio_user_type_buf[e_GPIO_LED0].set_high_func = bsp_gpio_set_led0_high;
//     bsp_gpio_user_type_buf[e_GPIO_LED0].set_low_func = bsp_gpio_set_led0_low;
//     /* led1 */
//     bsp_gpio_user_type_buf[e_GPIO_LED1].init_func = bsp_gpio_led1_init;
//     bsp_gpio_user_type_buf[e_GPIO_LED1].set_high_func = bsp_gpio_set_led1_high;
//     bsp_gpio_user_type_buf[e_GPIO_LED1].set_low_func = bsp_gpio_set_led1_low;

//     /* 初始化 */
//     for(tmp_i = (GPIO_USER_TYPE_ENUM)0; tmp_i < e_GPIO_NUM; ++tmp_i)
//     {
//         bsp_gpio_user_type_buf[tmp_i].init_func();
//     }
// }
