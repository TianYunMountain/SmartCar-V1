/*************************************************************************************
 * @File Name: app_led.c
 * @brief    LED使用的app程序
 * @Version : 1.0
 * @Create Date : 2024-12-02
 * @Author : TianyunV1 email : 3026007337@qq.com
 * 
 * @copyright Copyright (c) 2024 Tianyun Mountain
 * 
 * modification history :
 * Date:   Version:  Author:  Description:   
 *************************************************************************************/
#include "app_led.h"
#include "mw_led.h"
#include "stdint.h"
#include "stdio.h"	
#include "mw_soft_timer.h"

static uint8_t led_style_change_flag = 0;

void app_led_change_style_enable(void)
{
    led_style_change_flag = 1;
}

static uint8_t app_led_get_change_style_value(void)
{
    return led_style_change_flag;
}

void app_led_change_style_disable(void)
{
    led_style_change_flag = 0;
}

void app_led_marquee(void)
{
    static uint8_t tmp_state = 0;
    static uint32_t tmp_tick = 0;

    if(app_led_get_change_style_value() > 0)
    {
        app_led_change_style_disable();
        tmp_state = 0;
    }
    
    switch(tmp_state)
    {
        case 0:
            mw_get_led_obj(Led0).off();
            if(get_systick_ms() - tmp_tick > 500)
            {
                tmp_state = 1;
                tmp_tick = get_systick_ms();
            }
            break;
        case 1:
            mw_get_led_obj(Led0).on();
            if(get_systick_ms() - tmp_tick > 500)
            {
                tmp_state = 2;
                tmp_tick = get_systick_ms();
                // printf("123\n");
            }
            break;
        
        case 2:
            mw_get_led_obj(Led0).off();
            if(get_systick_ms() - tmp_tick > 500)
            {
                tmp_state = 1;
                tmp_tick = get_systick_ms();
            }
            break;
    }
}


