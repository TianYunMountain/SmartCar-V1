/*************************************************************************************
 * @File Name: main.c
 * @brief    
 * @Version : 1.0
 * @Create Date : 2024-12-02
 * @Author : TianyunV1 email : 3026007337@qq.com
 * 
 * @copyright Copyright (c) 2024 Tianyun Mountain
 * 
 * modification history :
 * Date:   Version:  Author:  Description:   
 *************************************************************************************/
#include "misc.h"

#include "app_led.h"

#include "mw_led.h"
#include "mw_bluetooth.h"

#include "bsp_gpio.h"
#include "bsp_timer.h"
#include "bsp_usart.h"


/*************************************************************************************
 * @brief    bsp init.
 * 
 * @warning  
 * @note     
 *************************************************************************************/
void bsp_init(void)
{
    // bsp_gpio_init();
    bsp_timer_init();
    // bsp_usart_1_init(115200);
}

void middleware_init(void)
{
    // led mw. init
    mw_led_drv_init();
    // bluetooth mw. init
    mw_bluetooth_drv_init();
}

/*************************************************************************************
 * @brief    Main function.
 * 
 * @return int         
 * @warning  
 * @note     
 *************************************************************************************/
int main(void)
{
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);

    bsp_init();

    middleware_init();

    while (1)
    {
        app_led_marquee();
    }
}
