#include "mw_bluetooth.h"

/* user external inc. */

#include "mw_soft_timer.h" 

#include "stm32f10x.h"

#include "public_diy.h"


#define BLUETOOTH_USART_PERIPH      (USART1)
// Initial the entity of bluetooth buf.
mw_bluetooth_t bluetooth_drv_buf[bluetooth_num];

/*************************************************************************************
 * @brief    Get the bluetooth object
 * @param[in/out] e_val
 * @return mw_bluetooth_t
 * 
 * @warning  
 * @note     
  *************************************************************************************/
mw_bluetooth_t mw_get_bluetooth_drv(bluetooth_type_enum e_val)
{
    if(e_val >= bluetooth_num)
    {
        while(1);
    }
    return bluetooth_drv_buf[e_val];
}

static int8_t mw_usart_init(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	
	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;//TX
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;//RX
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	
	USART_InitStructure.USART_BaudRate = 9600;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Tx | USART_Mode_Rx;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_Init(BLUETOOTH_USART_PERIPH, &USART_InitStructure);
	
	USART_ITConfig(BLUETOOTH_USART_PERIPH, USART_IT_RXNE, ENABLE);
	
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
	
	
	NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_Init(&NVIC_InitStructure);
	
	USART_Cmd(BLUETOOTH_USART_PERIPH, ENABLE);

    return RET_OK;
}

static int8_t mw_usart_deinit(void)
{
    USART_DeInit(BLUETOOTH_USART_PERIPH);
    return RET_OK;
}

static int8_t mw_send_one_byte(uint8_t uc_data)
{
    USART_SendData(BLUETOOTH_USART_PERIPH, uc_data);
    return RET_OK;
}

static uint8_t mw_receive_one_byte(void)
{
    return (uint8_t)(USART_ReceiveData(BLUETOOTH_USART_PERIPH));
}
/*************************************************************************************
 * @brief    bluetooth driver init.
 * 
 * @warning  
 * @note     
 *************************************************************************************/
void mw_bluetooth_drv_init(void)
{
/* hc06 init */
    // bluetooth driver type
    bluetooth_drv_buf[hc06].bluetooth_drv = hc06;
    // timer api
    bluetooth_drv_buf[hc06].timer_obj.pf_get_systick_ms = get_systick_ms;
    // usart api
    // bluetooth_drv_buf[hc06].bluetooth_usart.usart_type = ;
    bluetooth_drv_buf[hc06].bluetooth_usart.pf_init = mw_usart_init;
    bluetooth_drv_buf[hc06].bluetooth_usart.pf_deinit = mw_usart_deinit;
    bluetooth_drv_buf[hc06].bluetooth_usart.pf_recv_one_byte = mw_receive_one_byte;
    bluetooth_drv_buf[hc06].bluetooth_usart.pf_trans_one_byte = mw_send_one_byte;



}


