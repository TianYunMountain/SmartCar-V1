#ifndef __MW_BLUETOOTH_H__
#define __MW_BLUETOOTH_H__

#include "stm32f10x.h"

typedef enum
{
    hc06 = 0,
    bluetooth_num
}bluetooth_type_enum;

/* usart api define*/
typedef struct
{
    // void * usart_type;
    int8_t (*pf_init)(void);
    int8_t (*pf_deinit)(void);
    // tx rx function
    int8_t (*pf_trans_one_byte)(uint8_t tx_data);
    uint8_t (*pf_recv_one_byte)(void);
}system_usart_t;

/* timer api define */
typedef struct
{
    uint32_t (*pf_get_systick_ms)(void);
}system_timetick_t;

typedef struct
{
    // the type of bluetooth
    bluetooth_type_enum bluetooth_drv;
    // usart api
    system_usart_t bluetooth_usart;
    // timer api
    system_timetick_t timer_obj;
    void (*pf_init)(void);
    void (*pf_deinit)(void);
    void (*pf_send_bytes)(uint8_t * tx_buf, uint16_t len);
    void (*pf_recv_deal)(void);
}mw_bluetooth_t;



void mw_bluetooth_drv_init(void);

#endif
