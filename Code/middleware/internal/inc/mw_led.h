#ifndef __MW_LED_H__
#define __MW_LED_H__

#include "stm32f10x.h"

typedef enum
{
    Led0 = 0,

    led_num
}led_type_enum;

typedef struct
{
    led_type_enum led_drv;
    void (*init)(void);
    void (*on)(void);
    void (*off)(void);
}mw_led_t;

mw_led_t mw_get_led_obj(led_type_enum e_led_type);

void mw_led_drv_init(void);

#endif
