#ifndef __MW_SOFT_TIMER_H__
#define __MW_SOFT_TIMER_H__

#include "stdint.h"

uint32_t get_systick_ms(void);

void mw_soft_timer_user_systick_update(void);

#endif
