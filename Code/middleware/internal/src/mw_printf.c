#include "mw_printf.h"
#include "stdint.h"
#include "bsp_usart.h"
#include "stdio.h"

#define PRINTF_BUF_SIZE         (50u)

uint8_t mw_printf_buf[50] = {0};
static uint16_t mw_printf_cache_head = 0;
void mw_printf_insert_data(uint8_t val)
{
    mw_printf_buf[mw_printf_cache_head] = val;
    mw_printf_cache_head = (mw_printf_cache_head + 1) % PRINTF_BUF_SIZE;
    
}

