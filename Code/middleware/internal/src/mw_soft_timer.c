#include "mw_soft_timer.h"
#include "bsp_timer.h"


// 用户使用的systick时间
volatile static uint32_t systick_ms = 0;

uint32_t get_systick_ms()
{
    return systick_ms;
}

void mw_soft_timer_user_systick_update(void)
{
    ++systick_ms;
}



