/*************************************************************************************
 * @File Name: mw_led.c
 * @brief:
 * @Version : 1.0
 * @Create Date : 2024-12-02
 * @Author : TianyunV1 email : 3026007337@qq.com
 * 
 * @copyright Copyright (c) 2024 Tianyun Mountain
 * 
 * modification history :
 * Date:   Version:  Author:  Description:   
 *************************************************************************************/
#include "mw_led.h"
#include <stdio.h>
/* led pin define */
// LED0 PC13
#define IO_LED0_PORT        (GPIOC)
#define IO_LED0_RCC_PERIPH  (RCC_APB2Periph_GPIOC)
#define IO_LED0_PIN         (GPIO_Pin_13)

/* led middleware instantiation */
mw_led_t led_drv_buf[led_num];

/*************************************************************************************
 * @brief    Get the obj of Led.
 * 
 * @param[in/out] e_led_type
 * @return mw_led_t    
 * @warning  
 * @note     
 *************************************************************************************/
mw_led_t mw_get_led_obj(led_type_enum e_led_type)
{
    if(e_led_type >= led_num)
    {
        // invalid
        while(1);
    }

    return led_drv_buf[e_led_type];
}

/* led 0 start */
void mw_led0_init(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    
    RCC_APB2PeriphClockCmd(IO_LED0_RCC_PERIPH, ENABLE); //使能 PA 端口时钟

    GPIO_InitStructure.GPIO_Pin = IO_LED0_PIN;           //LED0-->PA.8 端口配置
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;    //推挽输出
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;   //IO 口速度为 50MHz
    GPIO_Init(IO_LED0_PORT, &GPIO_InitStructure);              //初始化 GPIOA.8
    // 初始化：高
    GPIO_SetBits(IO_LED0_PORT, IO_LED0_PIN);                     //PA.8 输出高
}

/*************************************************************************************
 *  Led0 start
**************************************************************************************/
void mw_led0_on(void)
{
    GPIO_SetBits(IO_LED0_PORT, IO_LED0_PIN);
}
void mw_led0_off(void)
{
    GPIO_ResetBits(IO_LED0_PORT, IO_LED0_PIN);
}
/*************************************************************************************
 *  Led0 end
**************************************************************************************/


/*************************************************************************************
 * @brief    Led driver installation.
 * 
 * @warning  
 * @note     
 *************************************************************************************/
void mw_led_drv_init(void)
{
    led_type_enum e_val;
    /* led0 installation */
    led_drv_buf[Led0].led_drv = Led0;
    led_drv_buf[Led0].init = mw_led0_init;
    led_drv_buf[Led0].on = mw_led0_on;
    led_drv_buf[Led0].off = mw_led0_off;
    /* init all led peripherals */
    for(e_val = Led0; e_val < led_num; e_val++)
    {
        led_drv_buf[e_val].init();
    }
}
